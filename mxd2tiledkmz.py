# -*- coding: utf-8 -*-
"""
Converts an ArcGis .mxd file into a tiled .kmz file containing JPG ground overlays

Input Parameters:

                    Type:       Nr.
INPUT_MXD_FILE:     File        0   The input mxd file
DATA_FRAME_NAME:    String      1   The used Data Frame - Default: Active
MAX_TILES:          int         2   Maximum number of tiles created
DRAW_ORDER          String      3   Draw order of the output kmz file
OUTPUT_KMZ_FILE:    Folder      4   Output kmz File
"""

# ======== IMPORTS/ENVIRONMENTS ========
import sys
import arcpy
import os
import xml.etree.ElementTree as ET
import shutil
import struct
import imghdr
import re

from glob import glob
from traceback import format_exc
from math import sqrt, ceil
from time import sleep
from tempfile import mkdtemp
from zipfile import ZipFile

__author__ = "Elias Wanko"
__email__ = "wankoelias@gmx.at"
__version__ = 0.3
__license__ = "GPL"

# avoid problems with multiple python installations
sys.path = sorted(sys.path, key=lambda k: "ArcGIS" in k, reverse=True)

# Allow overwriting output
arcpy.env.overwriteOutput = True

# Don't stop the script with the cancel button (Doesnt work pre ArcGis 10.4)
arcpy.env.autoCancelling = False

ARCGIS_VERSION = float(".".join(arcpy.GetInstallInfo()["Version"].split(".")[:2]))

# ======== Classes ========

class CancelledError(Exception):
    """
    Dummy Excepton for breaking the code when the cancel Button was pressed
    """
    pass

# ======== Functions ========

def get_image_size(fname):
    '''Determine the image type of fhandle and return its size.
    from draco
    Source: http://stackoverflow.com/a/20380514
    '''

    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24:
            return
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg':
            try:
                fhandle.seek(0) # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception: #IGNORE:W0703
                return
        else:
            return
        return width, height


def extent_from_jgw(jgw_path, x_size, y_size):
    """
    Calculates corner coordinates of a given raster jgw file and its x- and y- size

    :param jgw_path: Path to the jgw File
    :param x_size: Pixel count in x direction
    :param y_size: Pixel count in y direction
    :return: Dict containing maximum and minimum coordinates of the raster
    """

    with open(jgw_path, "r") as jgw_file:
        worldfile_list = jgw_file.read().split("\n")

    worldfile_list = [float(line.replace(",", ".")) for line in worldfile_list if line]

    x_pixel_size = worldfile_list[0]
    y_pixel_size = worldfile_list[3]
    center_x_ul = worldfile_list[4]
    center_y_ul = worldfile_list[5]

    extent = {}

    extent["X_MIN"] = center_x_ul - x_pixel_size/2
    extent["Y_MAX"] = center_y_ul + y_pixel_size/2

    extent["X_MAX"] = extent["X_MIN"] + x_size*x_pixel_size
    extent["Y_MIN"] = extent["Y_MAX"] + y_size*y_pixel_size

    return extent


def gcs_from_prj(layer):
    """
    Tries to get a GCS out of the prj file of a layer
    :param layer:
    :return: EPSG Code of the layer. None if failed
    """

    lyr_src = layer.dataSource

    if not os.path.isdir(lyr_src):
        return None

    files = glob(os.path.join(lyr_src, "*"))
    prj = [f for f in files if os.path.basename(f) == "prj.adf"]

    if prj:
        prj = prj[0]
    else:
        return None

    prj_content = ""
    with open(prj, "r") as prj_file:
        prj_content = prj_file.read()

    if not prj_content:
        return None

    match = re.search("\"EPSG\",([0-9]+)", prj_content)

    if not match:
        return None

    epsg = int(match.group(1))
    gcs = arcpy.SpatialReference(epsg).GCS.factoryCode

    if gcs:
        return gcs

    else:
        return None


def gcs_from_layer(layer):
    """
    Tries to get the GCS of a layer
    :param layer
    :return: EPSG Code of the layer. None if failed
    """

    layer_gcs = None

    try:
        layer_gcs = arcpy.Describe(layer.dataSource).SpatialReference.GCS.factoryCode
    except:
        pass

    if layer_gcs:
        return layer_gcs

    layer_gcs = gcs_from_prj(layer)

    return layer_gcs


def check_wgs84_transformation(data_frame, layer):
    """
    Checks if a Data Frame has a transformation from WGS84 to a layers GCS

    :param data_frame: Data Frame
    :param layer: Layer
    :return: Layer name and Layer GCS if transformation is missing,
             layer is a service layer or layer is broken
    """

    current_trs = data_frame.geographicTransformations
    suspect = []

    if layer.isBroken:
        return [layer.name, "broken"]

    if layer.isServiceLayer or not layer.supports("DATASOURCE"):
        return [layer.name, 'service']

    if not layer.dataSource:
        return [layer.name, 'service']

    layer_gcs = gcs_from_layer(layer)

    if not layer_gcs:
        return [layer.name, 'unknown']

    if layer_gcs != 4326:
        try:
            possible_trs = arcpy.ListTransformations(arcpy.SpatialReference(4326),
                                                     arcpy.SpatialReference(layer_gcs))

        except (RuntimeError, ValueError):
            possible_trs = []

        trs_missing = True
        for current_tr in current_trs:
            if current_tr in possible_trs:
                trs_missing = False

        if trs_missing:
            suspect = [layer.name, layer_gcs]

    return suspect


def checklayers(data_frame):
    """
    Checks all visible layer in a given Data Frame if they are are either broken,
    have a unknown GCS, or have a GCS with no set transformation to WGS84

    :param data_frame: Data Frame

    :return: list of layers that might cause problems.
             Each entry contains the layer name and its GCS.
    """

    layers = arcpy.mapping.ListLayers(data_frame)

    layer_suspects = []
    current_group = [None, None]

    for layer in layers:

        # Checking layers makes only sense for visible layers
        # check if layer is a group layer and if it isn't actually visible
        # continue if either one of this is true
        if layer.isGroupLayer:
            if current_group[0]:
                if (current_group[0] in layer.longName) and (current_group[0] != layer.longName):
                    if current_group[1]:
                        current_group = [layer.longName, layer.visible]
                    else:
                        current_group = [layer.longName, False]
                else:
                    current_group = [layer.longName, layer.visible]
            else:
                current_group = [layer.longName, layer.visible]

            continue
        else:
            if current_group[0]:
                if (current_group[0] in layer.longName) and (current_group[0] != layer.longName):
                    if not current_group[1]:
                        continue
                else:
                    current_group = [None, None]

        if not layer.visible:
            continue

        suspect = check_wgs84_transformation(data_frame, layer)

        if suspect and suspect not in layer_suspects:
            layer_suspects.append(suspect)

    return layer_suspects


def get_raster_size(ratio, fixed_width):
    """
    calculates size of the biggest raster for fixed ratio and
    maximum tile number either for fixed height or fixed width

    :param ratio:           float  Ratio of height and width
    :param fixed_width:     bool   True if ratio = height/width. False if ratio = width/height
    :return: width, height  tuple
    """

    subtract = 0
    while True:
        side_a = int(sqrt(MAX_TILES/ratio) - subtract) * 1024
        side_b = int(round(side_a * ratio, 0))

        if (side_a/1024) * ceil(side_b / 1024.0) <= MAX_TILES:
            break
        subtract += 1

    if fixed_width:
        return side_a, side_b
    else:
        return side_b, side_a


def check_cancel_status():
    """
    Checks if script got cancelled
    :return: Returns True if Script got cancelled.
             Default: False
             (Only works with ArcGis 10.4 and later)
    """
    if ARCGIS_VERSION >= 10.4:
        if arcpy.env.isCancelled:
            return True
    return False


def remove_path(*args):
    """
    Removes Tempoary Files/Folders. Prints a waring if an Element cant be deleted
    :param args: Any number of paths to files/folder
    """
    for path in args:

        try:
            if os.path.isfile(path):
                os.remove(path)

            if os.path.isdir(path):
                shutil.rmtree(path, True)

        except:
            arcpy.AddWarning("Failed to remove temporary file:\n{}\n"
                             "Please delete manually".format(path))

def mxd2tiledkmz():
    """
    Main function of this script
    """

    # ======== Preparations ========

    # Dummy Variables so the finally block doesn't fail
    data_frame = None
    mxd = None
    mxd_file = ""
    temp_folder = ""

    # Function to monitor Progress and stop the script if cancel button was pressed
    def progress(message=None, check_if_canceled=True):

        if message:
            arcpy.AddMessage(message)
            arcpy.SetProgressorLabel(message)

        if check_if_canceled:
            if check_cancel_status():
                raise CancelledError

    try:
        # Setting up the progress bar
        arcpy.SetProgressor('default', "Creating Garmin Custom Map")

        # Create Temporary Folder
        temp_folder = mkdtemp(prefix="mxd2tildedkmz_", dir=arcpy.env.scratchFolder)

        # Create Subfolders
        split_folder = os.path.join(temp_folder, "split")
        kml_folder = os.path.join(temp_folder, "kmz_content")
        os.makedirs(split_folder)
        os.makedirs(kml_folder)

        # Temporary File Paths
        map_jpeg = os.path.join(temp_folder, "map.jpg")
        kml_file = os.path.join(kml_folder, "doc.kml")
        extent_shape = os.path.join(temp_folder, "extent.shp")
        extent_shape_wgs84 = os.path.join(temp_folder, "extent_wgs84.shp")

        mxd_file = os.path.join(os.path.dirname(INPUT_MXD_FILE), "~tmp_{}"
                                .format(os.path.basename(INPUT_MXD_FILE)))
        # If zhe script gets canceled, the temporary mxd file doesnt get deleted.
        # Prevent error in the next run by choosing a different name for the mxd file
        mxd_number = 0
        while os.path.exists(mxd_file):
            mxd_number += 1
            mxd_file = os.path.join(os.path.dirname(INPUT_MXD_FILE), "~tmp{}_{}"
                                    .format(mxd_number, os.path.basename(INPUT_MXD_FILE)))

        # Use copy of source mdx file
        shutil.copy(INPUT_MXD_FILE, mxd_file)
        mxd = arcpy.mapping.MapDocument(mxd_file)

        # Change view to 'PAGE_LAYOUT'
        mxd.activeView = 'PAGE_LAYOUT'
        mxd.save()

        map_name = os.path.splitext(os.path.basename(OUTPUT_KMZ_FILE))[0]

        # Set Data Frame
        data_frame = [x for x in arcpy.mapping.ListDataFrames(mxd)
                      if x.name == DATA_FRAME_NAME][0]

        # ======== Processing ========

        ## Checking Layers:
        progress("Checking Layers...")

        final_layer_warning = False
        final_service_warning = False

        try:
            layer_suspects = checklayers(data_frame)

        except:
            layer_suspects = []
            arcpy.AddWarning(format_exc())
            arcpy.AddWarning("Layer check failed!\n"
                             "Make sure the geographic coordinate systems (GCS) of all layers"
                             " is WGS_84 or geographic transformations to WGS_84 are set."
                             " Otherwise, this might cause a shift in the output file.")

        for suspect in layer_suspects:
            if suspect[1] == "unknown":
                gcs_name = "unknown GCS"

            elif suspect[1] == "broken":
                arcpy.AddWarning("Unable to draw broken layer '%s'" % (suspect[0]))

            elif suspect[1] == 'service':
                arcpy.AddWarning("Projection for service layer '%s' could not be identified."
                                 % (suspect[0]))
                final_service_warning = True

            else:
                gcs_name = arcpy.SpatialReference(suspect[1]).name

            if suspect[1] not in ["service", "broken"]:
                arcpy.AddWarning("Geographic transformation to WGS_84 was not found for"
                                 " layer '%s' (%s)" % (suspect[0], gcs_name))
                final_layer_warning = True

        if final_layer_warning:
            arcpy.AddWarning("\nThe geographic coordinate system (GCS) of one or more"
                             " layers differs from WGS_84 and no geographic transformation"
                             " to WGS_84 is set. This might cause a shift in the output file.")

        if final_service_warning:
            arcpy.AddWarning("\nThe geographic coordinate system (GCS) of one or more service"
                             " layers could not be identified. If the GCS differs from WGS_84"
                             " and no geographic transformation to WGS_84 was set, this might"
                             " cause a shift in the output file.")


        ## Identifying and Zomming to map extent if Data frame is not in WG84
        progress("Checking Extent...")

        # save extent of the Data Frame to get the same Area
        # after changing the Data Frame coordinate system to WGS84
        if data_frame.spatialReference.factoryCode != 4326:
            extent_list = [
                [data_frame.extent.XMin, data_frame.extent.YMin],
                [data_frame.extent.XMin, data_frame.extent.YMax],
                [data_frame.extent.XMax, data_frame.extent.YMax],
                [data_frame.extent.XMax, data_frame.extent.YMin],
            ]
            
            extent_geoms = arcpy.Polygon(
                arcpy.Array([arcpy.Point(p[0], p[1]) for p in extent_list])
            )

            # Export extent in map CRS
            arcpy.env.outputCoordinateSystem = data_frame.spatialReference
            arcpy.CopyFeatures_management(extent_geoms, extent_shape)
            arcpy.DefineProjection_management(extent_shape, data_frame.spatialReference)

            # Project it into WGS84
            # Check out transformations for the extent_shape if its GCS differs from WGS 84
            shape_trans = None
            if arcpy.Describe(extent_shape).spatialReference.GCS.factoryCode != 4326:

                available_trs = data_frame.geographicTransformations
                possible_trs = arcpy.ListTransformations(
                    arcpy.Describe(extent_shape).spatialReference,
                    arcpy.SpatialReference(4326)
                )
                filtered_trs = [x for x in possible_trs if x in available_trs]

                if filtered_trs:
                    shape_trans = filtered_trs[0]

            arcpy.Project_management(extent_shape, extent_shape_wgs84, "4326", shape_trans)

        else:
            extent_shape_wgs84 = None

        # Change Data Frame Projection to WGS 84
        data_frame.spatialReference = arcpy.SpatialReference(4326)
        arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(4326)


        # Zoom to extent
        if extent_shape_wgs84:
            ex = arcpy.Describe(extent_shape_wgs84).extent
            df_ratio = (ex.XMax-ex.XMin)/(ex.YMax-ex.YMin)
            data_frame.elementWidth = data_frame.elementHeight*df_ratio
            data_frame.extent = arcpy.Describe(extent_shape_wgs84).extent


        ## Exporting Map
        progress("Exporting Map...")

        # Determining output size

        # Calculate 2 possible sizes for the maximum tile number.
        # Then use the one with more pixels
        size_1 = get_raster_size(data_frame.elementHeight/data_frame.elementWidth, True)
        size_2 = get_raster_size(data_frame.elementWidth/data_frame.elementHeight, False)

        if size_1[0] * size_1[1] > size_2[0] * size_2[1]:
            map_width, map_height = size_1
        else:
            map_width, map_height = size_2

        # Export the map
        arcpy.mapping.ExportToJPEG(mxd, map_jpeg, data_frame,
                                   map_width, map_height, world_file=True)
        arcpy.DefineProjection_management(map_jpeg, arcpy.SpatialReference(4326))

        ## Splitting JPG into Tiles
        progress("Creating Tiles...")

        split_base = "split"
        split_format = "JPEG" if ARCGIS_VERSION >= 10.2 else "JPG"

        arcpy.SplitRaster_management(
            map_jpeg, split_folder, split_base, "SIZE_OF_TILE", split_format, "NEAREST", "1 1",
            "1024 1024", "0", "PIXELS"
        )

        ## Create kml File
        progress("Creating kml File...")

        # Parse generated JPG files
        #    Get list of JPG files created
        tilelist = []
        for jpg_file in glob(os.path.join(split_folder, "*.JPG")):
            tilelist.append({
                'ID': os.path.splitext(os.path.basename(jpg_file).split(split_base)[1])[0],
                'jpg_path': jpg_file,
                'jgw_path': os.path.splitext(jpg_file)[0] + ".JGw"
            })
            
        #   calculate the extent of each tile
        for tile in tilelist:

            tile_size = get_image_size(tile["jpg_path"])
            tile_extent = extent_from_jgw(tile["jgw_path"], tile_size[0], tile_size[1])

            tile['west'] = str(tile_extent["X_MIN"])
            tile['east'] = str(tile_extent["X_MAX"])
            tile['north'] = str(tile_extent["Y_MAX"])
            tile['south'] = str(tile_extent["Y_MIN"])


        # Create kml file
        root = ET.Element("kml", xmlns="http://earth.google.com/kml/2.2")
        tree = ET.ElementTree(root)

        folder = ET.SubElement(root, "Folder")
        ET.SubElement(folder, "name").text = map_name

        # Loop through tilelist
        for tile in tilelist:

            # Add elements
            g_overlay = ET.SubElement(folder, "GroundOverlay")
            ET.SubElement(g_overlay, "name").text = "Tile " + tile['ID']
            ET.SubElement(g_overlay, "drawOrder").text = DRAW_ORDER

            icon = ET.SubElement(g_overlay, "Icon")
            ET.SubElement(icon, "href").text = tile['ID']+".jpg"

            box = ET.SubElement(g_overlay, "LatLonBox")
            ET.SubElement(box, "north").text = tile['north']
            ET.SubElement(box, "south").text = tile['south']
            ET.SubElement(box, "east").text = tile['east']
            ET.SubElement(box, "west").text = tile['west']

            # Copy and rename the jpg
            os.rename(tile["jpg_path"], os.path.join(kml_folder, tile['ID']+".jpg"))

        # Write Element tree into the kml file
        tree.write(kml_file, encoding="UTF-8")


        ## Create kmz file output
        progress("Creating kmz File...")

        with ZipFile(OUTPUT_KMZ_FILE, mode='w') as zip_f:
            for f in os.listdir(kml_folder):
                zip_f.write(os.path.join(kml_folder, f), arcname=f)

            arcpy.AddMessage("Result: " + OUTPUT_KMZ_FILE)


    except CancelledError:
        arcpy.AddMessage("Script got cancelled")

    except:
        if check_cancel_status():
            arcpy.AddMessage("Script got cancelled")

        else:
            arcpy.AddError("Script failed")
            arcpy.AddError(format_exc())

    finally:

        # Delete temporary files
        progress("Deleting temporary files...", check_if_canceled=False)
        sleep(2)
        del mxd
        del data_frame
        remove_path(mxd_file, temp_folder)


if __name__ == "__main__":

    INPUT_MXD_FILE = arcpy.GetParameterAsText(0)
    DATA_FRAME_NAME = arcpy.GetParameterAsText(1)
    MAX_TILES = int(arcpy.GetParameterAsText(2))
    DRAW_ORDER = arcpy.GetParameterAsText(3)
    OUTPUT_KMZ_FILE = arcpy.GetParameterAsText(4)

    mxd2tiledkmz()