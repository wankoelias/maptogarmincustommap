# ** Map To Garmin Custom Map**


ArcGIS tool that converts a Data Frame of an ArcGIS Map file (.mxd) into a tiled kmz file containing JPG ground overlays. 
The resulting kmz file can be used as a "Garmin Custom Map" on Garmin GPS devices.

Kmz Files use the geographic coordinate system WGS84.To avoid shifts in your output kmz file, 
it is recommended to set transformations to WGS84 for all other geographic coordinate systems used in your exported Data Frame. 
For layers originating in other geographic coordinate systems without available transformations to WGS84, a warning message will be displayed.

Tested for ArcGIS Verisons 10.1, 10.2, 10.3, 10.4

Feel free to use, change and improve this tool.
Please report bugs to wankoelias@gmx.at

## Download
Select "Downloads" in the section to the left. Select "Download repository".
Unpack and open "Map To Garmin Custom Map.tbx" in ArcCatalog
---
# Input Parameters


## Map File (.mxd) 

Input Map Document


## Data Frame 


Input Data Frame

Set the coordinate system of the Input Data Frame to WGS84 to ensure the output extent exactly matches the extent of the Data Frame.


## Maximum number of tiles 


Maximum number of tiles the output kmz file should contain. Each tile has a maximum resolution of 1 Megapixel. 
Increasing the maximum number of tiles will increase the accuracy of the output file, but will also result in a higher file size.


Note: Most Garmin GPS Devices don't support more than 100 Tiles.


## Draw Order 


States the order in which the output layer will be drawn on Garmin GPS Devices. 
Layers with a higher draw order will be drawn on top of layers with lower draw order. 
If the draw order is greater or equal to 50, the output layer will be drawn above most vector layers.


## Output (.kmz) 

Output kmz File

---
# Compatible Garmin GPS devices

* Alpha 100
* Astro 320
* BMW Motorrad Navigator V
* Colorado series
* Dakota series
* Edge 800/810/1000
* Epix
* eTrex 20/30 and 20x/30x
* eTrex Touch series
* GPSMAP 62 series
* GPSMAP 64 series
* GPSMAP 78 series
* Montana series
* Monterra
* Oregon series
* Rino 600 series
* zumo 395
* zumo 590
* zumo 595